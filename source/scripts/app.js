/**
 * Created by php on 23.05.16.
 */
var app = angular.module('plunker', ['ui.bootstrap']);

app.controller('MainCtrl', function($scope,$q) {
    $scope.name = 'World';
    $scope.values = {
        date1: new Date()
    };

    var dateDisableDeferred =  $q.defer();
    $scope.dateDisablePromise = dateDisableDeferred.promise;

    var currentDay = new Date().getDay();
    var disableModeIsLessThan = true;

    $scope.toggleDisableMode = function() {
        dateDisableDeferred.notify(new Date().getTime());
        disableModeIsLessThan = !disableModeIsLessThan;
    };

    $scope.isDateDisabled = function(date,mode) {
        if (disableModeIsLessThan) {
            if (date.getDay()<currentDay) {
                return true;
            } else {
                return false;
            }
        } else {
            if (date.getDay()>currentDay) {
                return true;
            } else {
                return false;
            }
        }

    };
});

app.directive('jmDpRefreshView',function() {
    var noop = function(){};
    var refreshDpOnNotify = function (dpCtrl) {
        return function() {
            dpCtrl.refreshView();
        };
    };
    return {
        require: 'datepicker',
        link: function(scope,elem,attrs,dpCtrl) {
            var refreshPromise = scope[attrs.jmDpRefreshView];
            refreshPromise.then(noop,noop,refreshDpOnNotify(dpCtrl));
        }
    };
});

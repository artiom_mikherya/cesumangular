/**
 * Created by php on 24.05.16.
 */
var myApp = angular.module('myApp', []);

myApp.controller('MyCtrl',MyCtrl);


myApp.directive('myDatepicker', function ($parse) {
    return function (scope, element, attrs, controller) {
        var ngModel = $parse(attrs.ngModel);
        $(function(){
            element.datepicker({
                showOn:"both",
                changeYear:true,
                changeMonth:true,
                dateFormat:'dd-mm-yy',
                // datesDisabled: ["25-05-2016", "26-05-2016"],
                // maxDate: new Date(),
                yearRange: '2014:2020',
                // regional: 'ru',
                onSelect:function (dateText, inst) {
                    scope.$apply(function(scope){
                        // Change binded variable
                        ngModel.assign(scope, dateText);
                    });
                }
            });
        });
    }
});

$.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );

// $('.datepicker').datepicker()
//     .on(picker_event, function(e) {
//         // `e` here contains the extra attributes
//     });

/** Days to be disabled as an array */
var disableddates = ["25-05-2016", "26-05-2016", "27-05-2016", "25-06-2016"];

function DisableSpecificDates(date) {

    var m = date.getMonth();
    var d = date.getDate();
    var y = date.getFullYear();

    // First convert the date in to the mm-dd-yyyy format
    // Take note that we will increment the month count by 1
    var currentdate = (m + 1) + '-' + d + '-' + y ;

    // We will now check if the date belongs to disableddates array
    for (var i = 0; i < disableddates.length; i++) {

        // Now check if the current date is in disabled dates array.
        if ($.inArray(currentdate, disableddates) != -1 ) {
            return [false];
        }
    }

}

//
// $("#dp").datepicker({
//         beforeShowDay: DisableSpecificDates
// });

function MyCtrl($scope) {

    $scope.userInfo = {
        person: {
            // mDate: today
        }
    };
}
//
$("#dp").datepicker({
    onSelect: function(dateText, inst) {
        var date = $(this).val();
        var time = $('#time').val();
        $(this).change();
        alert(date);
        display("SDFSD:FLJS:DLFJS:LDJS:LFJ:SLDJV");
    }
}).on("change", function() {
    alert("Got change event from field");
});
    function display(msg) {
        $("<p>").html(msg).appendTo(document.body);
    }

/**
 * Created by php on 23.05.16.
 */

angular.module('ngdemo', ['ui.bootstrap']).

    controller('DatepickerDemoCtrl', function ($scope) {
    $scope.today = function() {
        $scope.dt = new Date();

    };
    $scope.today();

    load_cities = function() {
        return load_popups_data();
    };

    var selected_id = oopt[selected_polygon_name]._id;
    load_popups_data = function() {
        return $.getJSON('data/data.json', function(data) {
            return popups_data = data.data;
        });
    };

    var getDateFromData = function (selected_id) {
        var current_popup_data, datesArray;
        current_popup_data = {};
        for (j = 0, len = popups_data.length; j < len; j++) {
            dta = popups_data[j];
            if (dta.id === _id) {
                current_popup_data = dta;
            }
            datesArray = current_popup_data.date;
            return datesArray;
        }
    };
    
    $scope.dateOptions = {
        // dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: new Date(2016, 5, 22),
        minDate: new Date(),
        startingDay: 1,
        sideBySide : true

    };

    $scope.clear = function() {
        $scope.dt = null;
    };

    $scope.options = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true,
    };


    Date.prototype.addDays = function(days) {
        var dat = new Date(this.valueOf())
        dat.setDate(dat.getDate() + days);
        return dat;
    }

    function getDates(startDate, stopDate) {
        var dateArray = new Array();
        var currentDate = startDate;
        while (currentDate <= stopDate) {
            dateArray.push(currentDate.valueOf)
            currentDate = currentDate.addDays(1);
        }
        return dateArray;
    }


    var availableDates = ["12-12-2016","13-12-2016"];


    function available(date) {
        dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
        if ($.inArray(dmy, availableDates) != -1) {
            return true;
        } else {
            return false;
        }
    }


    $scope.disabled = function(date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 || !available(date)) );
    };



    $scope.toggleMin = function() {
        $scope.options.minDate = $scope.options.minDate ? null : new Date();
    }

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];


    $scope.setDate = function(year, month, day) {
        $scope.dt = new Date(year, month, day);
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date(tomorrow);
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
    ];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }

    // $(".date-picker").on("change", function () {
    //     var id = $(this).attr("id");
    //     var val = $("label[for='" + id + "']").text();
    //     $("#msg").text(val + " changed");
    // });
    var _getAllFilesFromFolder = function(dir) {

        var filesystem = require("fs");
        var results = [];

        filesystem.readdirSync(dir).forEach(function(file) {

            file = dir+'/'+file;
            var stat = filesystem.statSync(file);

            if (stat && stat.isDirectory()) {
                results = results.concat(_getAllFilesFromFolder(file))
            } else results.push(file);

        });

        return results;

    };
});


